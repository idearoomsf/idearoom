import './routes.js';
import './useraccounts-configuration.js';
import './configuration.js';
import './helpers.js';

import '/imports/api/Courses/courses.js';
import '/imports/api/CoursesAdmin/coursesAd.js';
import '/imports/api/Debates/debates.js';
import '/imports/api/Schools/schools.js';
import '/imports/api/Files/files.js';
import '/imports/api/Files/image.js';
import '/imports/api/Files/files.js';
