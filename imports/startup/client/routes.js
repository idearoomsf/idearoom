import { Template } from 'meteor/templating';
import { FlowRouter } from 'meteor/kadira:flow-router';
import { BlazeLayout } from 'meteor/kadira:blaze-layout';


// Import to load these templates
import '/imports/ui/client/layouts/Layout.js';
import '/imports/ui/client/layouts/AdminMenu.html';

import '/imports/ui/client/pages/HomePage.js';

import '/imports/ui/client/pages/Instructor/InstructorPage.js';
import '/imports/ui/client/pages/Instructor/InstructorDebatesPage.js';

import '/imports/ui/client/pages/Student/StudentPage.js';
import '/imports/ui/client/pages/Student/StudentDebatesPage.js';
import '/imports/ui/client/pages/Student/StudentDebateDetailsPage.js';


import '/imports/ui/client/pages/Admin/AdminPage.js';
import '/imports/ui/client/pages/Admin/AdminSchoolsPage.js';
import '/imports/ui/client/pages/Admin/AdminCoursesPage.js';
import '/imports/ui/client/pages/Admin/AdminUsersPage.js';
import '/imports/ui/client/pages/Admin/AdminCoursesADPage.js';



FlowRouter.route('/', {
    name: "home",
    action: function(params, queryParams) {
        BlazeLayout.render('Layout', { main: 'HomePage' });
    },
    triggersEnter: [function(context, redirect) {
        console.log('running / trigger');
        Tracker.autorun(function() { //Reload each time a dependencie change, we wait for Meteor.user() to be loaded
            if(Roles.getRolesForUser(Meteor.userId())[0]) {
                console.log('Redirecting user')
                var route = '/' + Roles.getRolesForUser(Meteor.userId())[0];
                FlowRouter.go(route);
            }
        });
    }]
});


// Instructor Group
var instructorRoutes = FlowRouter.group({
    prefix: '/instructor',
    name: 'instructor',
    triggersEnter: [function(context, redirect) {
        console.log('running instructor group triggers');
        Tracker.autorun(function() { //Reload each time a dependencie change, we wait for Meteor.user() to be loaded
            if(Roles.getRolesForUser(Meteor.userId())[0] && !Roles.userIsInRole(Meteor.userId(), ['instructor'])) {
                console.log('Redirecting user')
                var route = '/' + Roles.getRolesForUser(Meteor.userId())[0];
                FlowRouter.go(route);
            }
        });
    }]
});

// handling /instructor route
instructorRoutes.route('/', {
    action: function() {
        BlazeLayout.render('Layout', { main: 'InstructorPage' });
    },
    triggersEnter: [function(context, redirect) {
        console.log('running /instructor trigger');
    }]
});

// handling /instructor/course
instructorRoutes.route('/course/:courseId', {
    action: function() {
        BlazeLayout.render('Layout', { main: 'InstructorDebatesPage' });
    },
    triggersEnter: [function(context, redirect) {
        console.log('running /instructor/course trigger');
    }]
});


// Student Group
var studentRoutes = FlowRouter.group({
    prefix: '/student',
    name: 'student',
    triggersEnter: [function(context, redirect) {
        console.log('running student group triggers');
        Tracker.autorun(function() { //Reload each time a dependencie change, we wait for Meteor.user() to be loaded
            if(Roles.getRolesForUser(Meteor.userId())[0] && !Roles.userIsInRole(Meteor.userId(), ['student'])) {
                console.log('Redirecting user')
                var route = '/' + Roles.getRolesForUser(Meteor.userId())[0];
                FlowRouter.go(route);
            }
        });
    }]
});

// handling /student route
studentRoutes.route('/', {
    action: function() {
        BlazeLayout.render('Layout', { main: 'StudentPage' });
    },
    triggersEnter: [function(context, redirect) {
        console.log('running /student trigger');
    }]
});

// handling /student/course
studentRoutes.route('/course/:courseId', {
    action: function() {
        BlazeLayout.render('Layout', { main: 'StudentDebatesPage' });
    },
    triggersEnter: [function(context, redirect) {
        console.log('running /student/course trigger');
    }]
});

// handling /student/debate
studentRoutes.route('/debate/:debateId', {
    action: function() {
        BlazeLayout.render('Layout', { main: 'StudentDebateDetailsPage' });
    },
    triggersEnter: [function(context, redirect) {
        console.log('running /student/debate trigger');
    }]
});


// Admin Group
var adminRoutes = FlowRouter.group({
    prefix: '/admin',
    name: 'admin',
    triggersEnter: [function(context, redirect) {
        console.log('running admin group triggers');
        Tracker.autorun(function() { //Reload each time a dependencie change, we wait for Meteor.user() to be loaded
            if(Roles.getRolesForUser(Meteor.userId())[0] && !Roles.userIsInRole(Meteor.userId(), ['admin'])) {
                console.log('Redirecting user')
                var route = '/' + Roles.getRolesForUser(Meteor.userId())[0];
                FlowRouter.go(route);
            }
        });
    }]
});

// handling /admin route
adminRoutes.route('/', {
    action: function() {
        BlazeLayout.render('Layout', { main: 'AdminPage' });
    },
    triggersEnter: [function(context, redirect) {
        console.log('running /admin trigger');
    }]
});

// handling /admin/schools route
adminRoutes.route('/schools', {
    action: function() {
        BlazeLayout.render('Layout', { main: 'AdminSchoolsPage' });
    },
    triggersEnter: [function(context, redirect) {
        console.log('running /admin/schools trigger');
    }]
});

// handling /admin/courses route
adminRoutes.route('/courses', {
    action: function() {
        BlazeLayout.render('Layout', { main: 'AdminCoursesPage' });
    },
    triggersEnter: [function(context, redirect) {
        console.log('running /admin/courses trigger');
    }]
});

// handling /admin/users route
adminRoutes.route('/users', {
    action: function() {
        BlazeLayout.render('Layout', { main: 'AdminUsersPage' });
    },
    triggersEnter: [function(context, redirect) {
        console.log('running /admin/users trigger');
    }]
});

// handling /admin/coursesAd route
adminRoutes.route('/coursesAd', {
    action: function () {
        BlazeLayout.render('Layout', { main: 'AdminCoursesADPage' });
    },
    triggersEnter: [function (context, redirect) {
        console.log('running /admin/coursesAd trigger');
    }]
})