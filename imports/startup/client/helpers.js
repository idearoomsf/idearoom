Template.registerHelper('FormatDateTime', function(date){
	if(date) {
		return moment(date).format("DD/MM/YYYY HH:mm");
		// return moment(date).format("MM/DD/YYYY HH:mm");
	}
});

Template.registerHelper('FormatDate', function(date){
	if(date) {
		return moment(date).format("DD/MM/YYYY");
		// return moment(date).format("MM/DD/YYYY");
	}
});

Template.registerHelper('not', function(a){
	return !a;
});

Template.registerHelper('equals', function (a, b) {
	return a === b;
});

Template.registerHelper('gt', function (a, b) {
	return a > b;
});

Template.registerHelper('lt', function (a, b) {
	return a < b;
});