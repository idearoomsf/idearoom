import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base';
import { FlowRouter } from 'meteor/kadira:flow-router';

Accounts.ui.config({
    requestPermissions: {},
    extraSignupFields: [{
        fieldName: 'role',
        showFieldLabel: false,      // If true, fieldLabel will be shown before radio group
        fieldLabel: 'Role',
        inputType: 'radio',
        radioLayout: 'inline',    // It can be 'inline' or 'vertical'
        data: [
            {                    // Array of radio options, all properties are required
                id: 1,                  // id suffix of the radio element
                label: 'Instructor',          // label for the radio element
                value: 'instructor'              // value of the radio element, this will be saved.
            },
            {
                id: 2,
                label: 'Student',
                value: 'student',
            },
            {
                id: 3,
                label: 'Admin',
                value: 'admin',
            }
        ],
        visible: true
    }]
});

var userLanguage = navigator.language || navigator.userLanguage;
if(userLanguage == "fr" || userLanguage=="fr-FR"){
    accountsUIBootstrap3.setLanguage('fr');
}
else {
    accountsUIBootstrap3.setLanguage('en');
}

Accounts.config({
  forbidClientAccountCreation : true
});

Accounts.onLogin(function() {
    if(Roles.userIsInRole(Meteor.user(), ['instructor'])) {
        FlowRouter.go("/instructor");
    }
    else if(Roles.userIsInRole(Meteor.user(), ['student'])) {
        FlowRouter.go("/student");
    }
    else if(Roles.userIsInRole(Meteor.user(), ['admin'])) {
        FlowRouter.go("/admin");
    }
    else {
        FlowRouter.go("/");
    }
});