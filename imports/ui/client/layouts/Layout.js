import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating'

import './Layout.html';
import './Layout.css';

Template.Layout.onCreated(function bodyOnCreated() {
    var self = this;
    self.autorun(function() {
        if(Meteor.userId()) {
            self.subscribe('courses');
        }
    });
});


Template.Layout.helpers({
    courses() {
        return Courses.find();
    },
    section() {
        if (Roles.userIsInRole(Meteor.userId(), ['admin'])) {
            return 'admin';
        }
        else if (Roles.userIsInRole(Meteor.userId(), ['instructor'])) {
            return 'instructor';
        }
        else if (Roles.userIsInRole(Meteor.userId(), ['student'])) {
            return 'student';
        }
        else {
            return 'unknown';
        }
    },
    courseId() {
        if(FlowRouter.getParam('courseId')) {
            return FlowRouter.getParam('courseId');
        }
        else if(FlowRouter.getParam('debateId')) {
            let debate = Debates.findOne({ _id : FlowRouter.getParam('debateId')});

            if(debate) {
                return debate.courseId;
            }
        }
    }
});