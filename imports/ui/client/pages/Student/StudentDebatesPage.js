import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating'

import './StudentDebatesPage.html';
import './StudentDebatesPage.css';

Template.StudentDebatesPage.onCreated(function bodyOnCreated() {
    var self = this;
    self.autorun(function() {
        self.subscribe('courseParticipants', FlowRouter.getParam('courseId'));
        self.subscribe('courses');
        self.subscribe('debates');
    });
});

Template.StudentDebatesPage.helpers({
    course() {
        return Courses.findOne( { _id : FlowRouter.getParam('courseId') } );
    },
    debates() {
        return Debates.find( { courseId: FlowRouter.getParam('courseId') } );
    },
    debatesCount() {
        return Debates.find( { courseId: FlowRouter.getParam('courseId') } ).count();
    },
    myDebatesCount() {
        return Debates.find(
            {
                courseId: FlowRouter.getParam('courseId'),
                "participants.participantId" : Meteor.userId()
            }
        ).count();
    },
    courseParticipants() {
        return Meteor.users.find(
            {
                roles : "student"
            },
            {
                fields: {
                    "profile.firstName": true,
                    "profile.lastName": true,
                }
            }
        );
    }
});

Template.StudentDebatesPage.events({
    'click #my-debates-tab': function(event) {
        $('#all-debates-tab').removeClass('active');
        $(event.currentTarget).addClass('active');
        $('.debate-list-row[data-participant="false"]').hide();
    },
    'click #all-debates-tab': function(event) {
        $('#my-debates-tab').removeClass('active');
        $(event.currentTarget).addClass('active');
        $('.debate-list-row').show();
    },
    'click .debate-list-row': function(event) {
        FlowRouter.go('/student/debate/:debateId', { debateId: $(event.currentTarget).attr('data-id') });
    }
});