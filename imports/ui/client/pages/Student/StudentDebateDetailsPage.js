import {
    Meteor
}
    from 'meteor/meteor';
import {
    Template
}
    from 'meteor/templating';
import {
    Session
}
    from 'meteor/session';
import './StudentDebateDetailsPage.html';
import './StudentDebateDetailsPage.css';
Template.StudentDebateDetailsPage.onCreated(function bodyOnCreated() {
    var self = this;
    self.autorun(function () {
        self.subscribe('debates');
        self.subscribe('courses');
        self.subscribe('courseParticipantsFromDebate', FlowRouter.getParam('debateId'));
        self.subscribe('fileCollection');
        self.subscribe('files');
    });
    if (Session.get('timeout')) {
        clearTimeout(Session.get('timeout'));
    }
    if(Session.get('countDown')) {
        clearInterval(Session.get('countDown'));
    }
    setTimeout(function () {
        var debate = Debates.findOne({
            _id: FlowRouter.getParam('debateId')
        });
        if (debate && !debate.isPaused()) {
            var now = new Date()
            var timeout = setTimeout(function () {
                if (!Session.get('messagePosted')) {
                    Meteor.call('postMessage', FlowRouter.getParam('debateId'), Session.get('message'), true);

                    var userLanguage = navigator.language || navigator.userLanguage;
                    console.log(userLanguage);
                    if(userLanguage == "fr" || userLanguage=="fr-FR"){
                        sAlert.success('Votre message a bien été publié', {position: 'bottom-right'});
                    }
                    else {
                        sAlert.success('Your message is posted successfully', {position: 'bottom-right'});
                    }
                }
            }, debate.round().endDate.getTime() - now.getTime() - 5000)
            Session.set('timeout', timeout)
            var countDown = setInterval(function() {
                var now = new Date()
                var remaining = Math.floor((debate.round().endDate.getTime() - now.getTime() - 5000) /1000);
                if((remaining / 3600) < 10){
                    var hours = '0' + parseInt(remaining / 3600);
                }
                else {
                    var hours = parseInt(remaining / 3600);
                }
                if((remaining - (hours * 3600 ))/60 < 10) {
                    var minutes = '0' + parseInt((remaining - (hours * 3600 ))/60);
                } else {
                    var minutes = parseInt((remaining - (hours * 3600 ))/60);
                }
                if((remaining - (hours * 3600 ) - (minutes * 60)) < 10) {
                    var seconds = '0' + (remaining - (hours * 3600 ) - (minutes * 60))%60;
                } else {
                    var seconds = (remaining - (hours * 3600 ) - (minutes * 60)) % 60;
                }
                if(parseInt(remaining / 3600) == 0 && minutes < 5) {
                    $('#countDown').css('color', 'red')
                    $('#countDown').css('font-size', '50px')
                }
                $('#countDown').html(hours + ' : ' + minutes + ' : ' + seconds);
            }, 1000)
            Session.set('countDown', countDown)
        }
    }, 1000)
});
Template.registerHelper('and',(a,b)=>{
    return a && b;
});
Template.registerHelper('or',(a,b)=>{
    return a || b;
});
Template.StudentDebateDetailsPage.helpers({
    debate() {
        var debate = Debates.findOne({
            _id: FlowRouter.getParam('debateId')
        });
        if (debate) {
            return debate;
        }
    },
    chatLive(group) {
        var debate = Debates.findOne({
            _id: FlowRouter.getParam('debateId')
        });
        if (debate) {
            var participantsList = debate.participants;
            for (var k = 0; k < participantsList.length; k++) {
                if (participantsList[k].participantId == Meteor.userId()) {
                    var currentTeamNumber = participantsList[k].teamNumber;
                    var currentSubTeamNumber = participantsList[k].subTeamNumber;
                    break;
                }
            };
            var chatLive = debate.chatLive;
            if (chatLive) {
                var chatLiveTeam = chatLive.filter(message => message.team == currentTeamNumber);
                if (chatLiveTeam) {
                    for (var i = 0; i < chatLiveTeam.length; i++) {
                        var author = Meteor.users.findOne(chatLiveTeam[i].author).profile;
                        if (chatLiveTeam[i].author == Meteor.userId()) {
                            chatLiveTeam[i].isCurrentUser = true;
                        }
                        else {
                            chatLiveTeam[i].isCurrentUser = false;
                        };
                        chatLiveTeam[i].author = author.firstName + ' ' + author.lastName;
                    };
                }
                var chatLiveSubteam = chatLiveTeam.filter(message => message.subteam == currentSubTeamNumber);
                if (group == 'subteam') {
                    var messages = chatLiveSubteam.filter(message => message.destination == 'subteam')
                    setTimeout(function () {
                        $('#discussion-subteam').scrollTop(1E10);
                    }, 200);
                }
                else {
                    var messages = chatLiveTeam.filter(message => message.destination == 'team')
                    setTimeout(function () {
                        $('#discussion-team').scrollTop(1E10);
                    }, 200);
                }
            }
        };

        return messages;
    },
    course(courseId, field) {
        var course = Courses.findOne({_id : courseId });
        if(course) {
            return course[field];
        }
    },
    debateInterventions() {
        var debate = Debates.findOne({ _id : FlowRouter.getParam('debateId')});
        if(debate) {
            var debateInterventions = new Mongo.Collection(null);
            for (var i in debate.participants) {
                var participant = debate.participants[i];
                for (var j in participant.interventions) {
                    debateInterventions.insert({
                        message: participant.interventions[j].text,
                        messageDate: participant.interventions[j].date,
                        teamNumber: participant.teamNumber,
                        author: Meteor.users.findOne(participant.participantId).profile
                    });
                }
            }

            for (var i in debate.instructorInterventions) {
                var intervention = debate.instructorInterventions[i];
                debateInterventions.insert({
                    message: intervention.text,
                    messageDate: intervention.date,
                    teamNumber: 10,
                    author: Meteor.users.findOne(intervention.instructorId).profile
                })
            }

            return debateInterventions.find({}, {sort: {messageDate: 1}});
        }
    },
    speakers() {
        var debate = Debates.findOne({ _id : FlowRouter.getParam('debateId')});

        if(debate) {
            var now = new Date();

            if(debate.isStarted() && !debate.isFinished()) {
                var round = debate.round();

                var currentSpeaker = debate.speaker(round.number);
                var currentViewers = debate.viewer(round.number);
                if (currentSpeaker.participantId == Meteor.userId()) {
                    var alreadySpoke = false;
                    for (var i in currentSpeaker.interventions) {
                        if (currentSpeaker.interventions[i].round == round.number) {
                            alreadySpoke = true;
                        }
                    }
                    var current = {
                        isCurrentSpeaker: true
                        , roundEndDate: round.endDate
                        , alreadySpoke: alreadySpoke
                        , isCurrentViewer: false
                    };
                }
                else {
                    var isViewer = false;
                    for (var j = 0; j < currentViewers.length; j++) {
                        if (currentViewers[j].participantId == Meteor.userId()) {
                            var current = {
                                isCurrentSpeaker: false
                                , currentSpeaker: currentSpeaker
                                , roundEndDate: round.endDate
                                , alreadySpoke: alreadySpoke
                                , isCurrentViewer: true
                            };
                            isViewer = true;
                        }
                    }
                    if (isViewer == false) {
                        var current = {
                            isCurrentSpeaker: false
                            , currentSpeaker: currentSpeaker
                            , roundEndDate: round.endDate
                            , isCurrentViewer: false
                        };
                    }
                }
                if (round.number == 8) {
                    var next = {
                        isLastSpeaker: true
                    }
                }
                else {
                    var next = {
                        isLastSpeaker: false
                    }
                    var nextSpeaker = debate.speaker(round.number + 1);
                    if (nextSpeaker.participantId == Meteor.userId()) {
                        next.isCurrentSpeaker = true;
                    }
                    else {
                        next.isCurrentSpeaker = false;
                        next.nextSpeaker = nextSpeaker;
                    }
                }
                return {
                    current: current
                    , next: next
                }
            }
        }
    },
    participantsProfile() {
        var debate = Debates.findOne({
            _id: FlowRouter.getParam('debateId')
        });
        if (debate) {
            var participantsList = debate.participants;
            var team1SubTeam1 = [];
            var team1SubTeam2 = [];
            var team2SubTeam1 = [];
            var team2SubTeam2 = [];

            for (var k = 0; k<participantsList.length; k++){
                var participant = Meteor.users.findOne(participantsList[k].participantId);
                if(participant) {
                    var participantProfile = participant.profile;
                    if(participantsList[k].teamNumber == 1 && participantsList[k].subTeamNumber == 1){
                        team1SubTeam1.push({
                            firstName: participantProfile.firstName,
                            lastName: participantProfile.lastName,
                            speaker: participantsList[k].speaker,
                            order: participantsList[k].order
                        });
                    } else if (participantsList[k].teamNumber == 1 && participantsList[k].subTeamNumber == 2){
                        team1SubTeam2.push({
                            firstName: participantProfile.firstName,
                            lastName: participantProfile.lastName,
                            speaker: participantsList[k].speaker,
                            order: participantsList[k].order
                        });
                    } else if (participantsList[k].teamNumber == 2 && participantsList[k].subTeamNumber == 1){
                        team2SubTeam1.push({
                            firstName: participantProfile.firstName,
                            lastName: participantProfile.lastName,
                            speaker: participantsList[k].speaker,
                            order: participantsList[k].order
                        });
                    } else if (participantsList[k].teamNumber == 2 && participantsList[k].subTeamNumber == 2){
                        team2SubTeam2.push({
                            firstName: participantProfile.firstName,
                            lastName: participantProfile.lastName,
                            speaker: participantsList[k].speaker,
                            order: participantsList[k].order
                        });
                    }
                }
            }
            return {
                team1SubTeam1: team1SubTeam1
                , team1SubTeam2: team1SubTeam2
                , team2SubTeam1: team2SubTeam1
                , team2SubTeam2: team2SubTeam2
            };
        }
    },
    isShow() {
        var debate = Debates.findOne({
            _id : FlowRouter.getParam('debateId')
        });

        if(debate && ((debate.isStarted() && !debate.isFinished()) || debate.isPaused())) {
            var round = debate.round();
            var currentSpeaker = debate.speaker(round.number);
            if(currentSpeaker.participantId == Meteor.userId()) {
                return true;
            }
        }
            
        return false;
    },
    links(){
        var debate = Debates.findOne({
            _id : FlowRouter.getParam('debateId')
        })
        if(debate && debate.linkResources) {
            var links = debate.linkResources.split("\r\n");
            return links;
        }
    },
    fileUploaded() {
        return FileFS.find();
    },
    fileUrl() {
        var file = FileFS.find({
            debateId : FlowRouter.getParam('debateId')
        }).map(function (fileFS) {
            return fileFS;
        });
        return file;
    }
});
Template.StudentDebateDetailsPage.events({
    'shown.bs.tab a[href="#debate"]': function () {
        $('#container-messages').scrollTop($('#all-messages').height());
    }
    , 'submit form': function (event) {
        event.preventDefault();
        Session.set('messagePosted', true);
        var message = event.target.message.value;
        Meteor.call('postMessage', FlowRouter.getParam('debateId'), message, false, function (error, result) {
            console.log(error);
            if (error) {
                $('textarea[name="message"]').parent().addClass('has-error');
                if (error.error = 'not-authorized') {
                    console.log(error.error);
                    $('#textarea-message-error').html('You can\'t participate in debate.');
                }
                else if (error.error == 'debate-finished') {
                    console.log(error.error);
                    $('#textarea-message-error').html('Debate is finished.');
                }
                else if (error.error == 'debate-not-begun') {
                    console.log(error.error);
                    $('#textarea-message-error').html('Debate has not begun.');
                }
                else if (error.error == 'not-speaker-turn') {
                    console.log(error.error);
                    $('#textarea-message-error').html('It is not user turn to speak.');
                }
                else if (error.error == 'already-spoke') {
                    console.log(error.error);
                    $('#textarea-message-error').html('You already spoke this round.');
                }
                else if (error.error == 'message-length') {
                    console.log(error.error);
                    $('#textarea-message-error').html('Your message is too ' + error.reason + '.');
                }
            }
            else {
                $('textarea[name="message"]').parent().removeClass('has-error');
                $('#textarea-message-error').html('');
                $('#message-length').html('');
                var userLanguage = navigator.language || navigator.userLanguage;
                console.log(userLanguage);
                if(userLanguage == "fr" || userLanguage=="fr-FR"){
                    sAlert.success('Votre message a bien été publié', {position: 'bottom-right'});
                }
                else {
                    sAlert.success('Your message is posted successfully', {position: 'bottom-right'});
                }
                event.target.reset();
            }
        });
    }
    , 'input textarea[name="message"]': function (event) {
        Session.set('message', event.target.value);
        var messageWithoutSpaces = event.target.value.replace(/\s/g, "");
        var length = messageWithoutSpaces.length;
        var debate = Debates.findOne({
            _id: FlowRouter.getParam('debateId')
        });
        /*var interventionLengths = debate.interventionLengths(debate.round().number);
         if (length < interventionLengths.min || length > interventionLengths.max) {
         $('#post-message').attr('disabled', true);
         }
         else {
         $('#post-message').attr('disabled', false);
         }
         if (length == 0) {
         $('#message-length').html('');
         }
         else if (length == 1) {
         $('#message-length').html('1 character ');
         }
         else if (length > 1) {
         $('#message-length').html(length + ' characters');
         }*/
        var interventionLengths = debate.interventionLengths(debate.round().number);
        if ((length < interventionLengths.min || length > interventionLengths.max) || debate.isPaused()) {
            $('#post-message').attr('disabled', true);
        }
        else {
            $('#post-message').attr('disabled', false);
        }
        if (length == 0) {
            $('#message-length').html('');
        }
        else if (length == 1) {
            $('#message-length').html('1 character ');
        }
        else if (length > 1) {
            $('#message-length').html(length + ' characters');
        }
    }
    , 'click #chat-live-subteam-open': function () {
        if ($('#chat-live-subteam').css("bottom") == "-430px") {
            $('#chat-live-subteam').css("bottom", "0px");
        }
        else {
            $('#chat-live-subteam').css("bottom", "-430px");
        }
    }
    , 'click #chat-live-team-open': function () {
        if ($('#chat-live-team').css("bottom") == "-430px") {
            $('#chat-live-team').css("bottom", "0px");
        }
        else {
            $('#chat-live-team').css("bottom", "-430px");
        }
    }
    , 'keypress #chat-message-subteam': function (event) {
        if (event.keyCode == 13) {
            event.preventDefault();
            var debate = Debates.findOne({
                _id: FlowRouter.getParam('debateId')
            });
            if (debate) {
                var participantsList = debate.participants;
                for (var k = 0; k < participantsList.length; k++) {
                    if (participantsList[k].participantId == Meteor.userId()) {
                        var currentTeamNumber = participantsList[k].teamNumber;
                        var currentSubTeamNumber = participantsList[k].subTeamNumber;
                        break;
                    }
                };
            };
            if (event.target.value != '') {
                Meteor.call('postChat', FlowRouter.getParam('debateId'), event.target.value, currentTeamNumber, currentSubTeamNumber, 'subteam', function (error, result) {
                    $('#chat-message-subteam').val('');
                });
            }
        }
    }
    , 'keypress #chat-message-team': function (event) {
        if (event.keyCode == 13) {
            event.preventDefault();
            var debate = Debates.findOne({
                _id: FlowRouter.getParam('debateId')
            });
            if (debate) {
                var participantsList = debate.participants;
                for (var k = 0; k < participantsList.length; k++) {
                    if (participantsList[k].participantId == Meteor.userId()) {
                        var currentTeamNumber = participantsList[k].teamNumber;
                        var currentSubTeamNumber = participantsList[k].subTeamNumber;
                        break;
                    }
                };
            };
            if (event.target.value != '') {
                Meteor.call('postChat', FlowRouter.getParam('debateId'), event.target.value, currentTeamNumber, currentSubTeamNumber, 'team', function (error, result) {
                    $('#chat-message-team').val('');
                });
            }
        }
    }
});