import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating'

import './AdminSchoolsPage.html';

Template.AdminSchoolsPage.onCreated(function bodyOnCreated() {
	var self = this;
	self.autorun(function() {
		self.subscribe("schools");
		self.subscribe("courses");
	});
});

Template.AdminSchoolsPage.helpers({
	schools() {
		return Schools.find({}, {sort: {name: 1}});
	},
	schoolsCount() {
		return Schools.find().count();
	}
});

