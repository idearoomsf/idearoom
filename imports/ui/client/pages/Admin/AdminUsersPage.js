import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating'

import './AdminUsersPage.html';

Template.AdminUsersPage.onCreated(function bodyOnCreated() {
	var self = this;
	self.autorun(function() {
		self.subscribe("users");
		self.subscribe("roles");
		self.subscribe("courses");
		self.subscribe("schools");
	});
});	

Template.AdminUsersPage.helpers({
	users() {
		return Meteor.users.find({}, {sort: {"emails.lastName": 1}});
	},
	school(schoolId, field) {
		var school = Schools.findOne(schoolId);
		if(school) {
			return school[field];
		}
	},
	courses(courseIds) {
		if(courseIds) {
			return Courses.find({_id : { $in: courseIds }});
		}
	},
	usersCount() {
		return Meteor.users.find().count();
	},
	rolesOptions() {
		return Meteor.roles.find({}, {sort: {name: 1}});
	},
	schoolsOptions() {
		return Schools.find({}, {sort: {name: 1}});
	},
	coursesOptions() {
		return Courses.find({}, {sort: {name: 1}});
	}
});

Template.AdminUsersPage.events({
	'submit form': function(event, template){
		event.preventDefault();

		var form = $(event.target).find('input, select').not('[value=""]').serializeJSON();

		Meteor.call('addUser', form);

		event.target.reset();
		$('#div-select-school').hide();
		$('#div-select-school select').val('');

		$('#div-select-course').hide();
		$('#div-select-course select').val([]);
	},

	'change select[name="roles[]"]': function(event) {

		if($(event.target).val() != "admin") {
			$('#div-select-school').show();
		}
		else {
			$('#div-select-school').hide();
			$('#div-select-school select').val('');

			$('#div-select-course').hide();
			$('#div-select-course select').val([]);
		}
		
	},

	'change select[name="profile[schoolId]"]': function(event) {
		var schoolId = $(event.target).val();

		if(schoolId != "") {
			$('#div-select-course option[schoolId!="' + schoolId + '"]').hide();
			$('#div-select-course option[schoolId!="' + schoolId + '"]').attr('disabled', true);
			$('#div-select-course option[schoolId="' + schoolId + '"]').show();
			$('#div-select-course option[schoolId="' + schoolId + '"]').removeAttr('disabled');
			$('#div-select-course option').first().show();
			$('#div-select-course option').first().removeAttr('disabled');

			$('#div-select-course').show();
			$('#div-select-course select').val([]);
		}
		else {
			$('#div-select-course').hide();
			$('#div-select-course select').val([]);
		}
	},
	'change #userUpload': function (event,template) {
        FS.Utility.eachFile(event, function(file) {
            console.log(event.target.value);
            var reader = new FileReader();
            reader.onload = function (event) {
				Meteor.call('importFile', event.target.value, reader.result,function (error, result) {
				})
            };

            reader.readAsBinaryString(file);
        });
        FlowRouter.go("/admin");
    }
});