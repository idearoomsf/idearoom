import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { Session } from 'meteor/session';

import './AdminCoursesADPage.html';

Template.AdminCoursesADPage.onCreated(function bodyOnCreated() {
    var self = this;
    self.autorun(function() {
        self.subscribe("courses");
        self.subscribe("coursesAd");
        self.subscribe("schools");
        self.subscribe("users");
        self.subscribe("roles");
    });
});

Template.AdminCoursesADPage.helpers({
    courses() {
        return CoursesAd.find({}, {sort: {name: 1}});
    },
    coursesCount() {
        return CoursesAd.find().count();
    },
    schoolsOptions() {
        schools = Schools.find({}, {sort: {name: 1}});

        schoolsOptions = [];

        schools.forEach(function(school) {
            schoolsOptions.push(
                {
                    value: school._id,
                    label: school.name
                }
            )
        });

        return schoolsOptions;
    },

    currentschoolId(){
        return Session.get("schoolId");
    },

    usersOptions(roleUser) {
        return Meteor.users.find({roles: roleUser}, {sort: {name: 1}});
    }

});

Template.AdminCoursesADPage.events({
    'submit form': function(event){
        event.preventDefault();

        var form = $(event.target).find('input, select').not('[value=""]').serializeJSON();

        Meteor.call('addCourseAd', form);

        event.target.reset();
    }
});