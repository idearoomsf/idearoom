import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { Session } from 'meteor/session';

import './AdminCoursesPage.html';

Template.AdminCoursesPage.onCreated(function bodyOnCreated() {
	var self = this;
	self.autorun(function() {
		self.subscribe("courses");
		self.subscribe("schools");
        self.subscribe("users");
        self.subscribe("roles");
	});
});

Template.AdminCoursesPage.helpers({
	courses() {
		return Courses.find({}, {sort: {name: 1}});
	},
	coursesCount() {
		return Courses.find().count();
	},
	schoolsOptions() {
		schools = Schools.find({}, {sort: {name: 1}});

		schoolsOptions = [];

		schools.forEach(function(school) {
			schoolsOptions.push(
				{
					value: school._id,
					label: school.name
				}
			)
		});

		return schoolsOptions;
	},
    
    currentschoolId(){
        return Session.get("schoolId");
    },
    
    usersOptions(roleUser) {
		return Meteor.users.find({roles: roleUser}, {sort: {name: 1}});
	}

});

Template.AdminCoursesPage.events({
    'submit form': function(event){
		event.preventDefault();

		var form = $(event.target).find('input, select').not('[value=""]').serializeJSON();

		Meteor.call('addCourse', form);

		event.target.reset();
		$('#div-select-instructor').hide();
		$('#div-select-instructor select').val('');

		$('#div-select-student').hide();
		$('#div-select-student select').val([]);
	},
    
    "change #div-select-school": function (event) {
    schoolId = $(event.target).val();
        if(schoolId != ""){
            $('#div-select-instructor option[schoolId!="' + schoolId + '"]').hide();
            $('#div-select-instructor option[schoolId!="' + schoolId + '"]').attr('disabled', true);
			$('#div-select-instructor option[schoolId="' + schoolId + '"]').show();
			$('#div-select-instructor option[schoolId="' + schoolId + '"]').removeAttr('disabled');
            
            $('#div-select-instructor').show();
			$('#div-select-instructor select').val([]);
            
            $('#div-select-student option[schoolId!="' + schoolId + '"]').hide();
            $('#div-select-student option[schoolId!="' + schoolId + '"]').attr('disabled', true);
			$('#div-select-student option[schoolId="' + schoolId + '"]').show();
			$('#div-select-student option[schoolId="' + schoolId + '"]').removeAttr('disabled');
            
            $('#div-select-student').show();
			$('#div-select-student select').val([]);
        }
        else {
			$('#div-select-instructor').hide();
			$('#div-select-instructor select').val([]);
            
            $('#div-select-student').hide();
			$('#div-select-student select').val([]);
		}
    },
    
    "input #studentSearch": function(event){
        studentSearch = new RegExp($(event.target).val(), 'i');
        
        $.map($('#div-select-student  option[schoolId="' + schoolId + '"]') ,function(option){
            var firstNameLastName = $(option).attr("studentFirstName") + " " + $(option).attr("studentLastName");
            var lastNameFirstName = $(option).attr("studentLastName") + " " + $(option).attr("studentFirstName");
            
           if($(option).attr("studentFirstName").search(studentSearch) != -1 || $(option).attr("studentLastName").search(studentSearch) != -1 || firstNameLastName.search(studentSearch) != -1 || lastNameFirstName.search(studentSearch) != -1){
               $(option).show();
           } else {
               $(option).hide();
           }
        })
        
},

 // Allow multiple selection without maintaining Ctrl key. Also, scroll back to position before selection
    "mousedown": function (e) {
        
    var el = e.target;
    if (el.tagName.toLowerCase() == 'option' && el.parentNode.hasAttribute('multiple')) {
        e.preventDefault();
        
        var scroll = $('#select-participants').scrollTop();

        e.target.selected = !e.target.selected;

        setTimeout(function() {
            $('#select-participants').scrollTop(scroll);
        }, 0);
    }
}
});