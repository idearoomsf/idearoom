import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating'

import './InstructorDebatesPage.html';
import './InstructorDebatesPage.css';

import '../../components/DebateForm.js';
import '../../components/DebateMessages.js';

Template.InstructorDebatesPage.onCreated(function bodyOnCreated() {
    var self = this;
    self.autorun(function() {
        self.subscribe('courseParticipants', FlowRouter.getParam('courseId'));
//		self.subscribe('courses'); Already subscribed in CourseMenu
        self.subscribe('debates');

        Session.set('debate', undefined);
        delete Session.keys.debate;
        Session.set('includedTemplate', 'DebateMessages');
        Session.set('includedTemplate', 'DebateForm');
    });
});

Template.InstructorDebatesPage.helpers({
    includedTemplate() {
        return Session.get('includedTemplate');
    },
    debate() {
        return Session.get('debate');
    },
    course() {
        return Courses.findOne( { _id : FlowRouter.getParam('courseId') } );
    },
    debates() {
        return Debates.find( { courseId: FlowRouter.getParam('courseId') } );
    },
    debatesCount() {
        return Debates.find( { courseId: FlowRouter.getParam('courseId') } ).count();
    },
    isReady(sub) {
        if(sub) {
            return FlowRouter.subsReady(sub);
        } else {
            return FlowRouter.subsReady();
        }
    }
});

Template.InstructorDebatesPage.events({
    'click #add-debate': function(event) {
        Session.set('debate', undefined);
        delete Session.keys.debate;
        Session.set('includedTemplate', 'DebateMessages');
        Session.set('includedTemplate', 'DebateForm');
    },
    'click .debate-link': function(event) {
        var debateId = event.target.getAttribute('debateId');
        Session.set('debate', debateId ); //Debates.findOne(debateId));
        Session.set('includedTemplate', 'DebateMessages');
    },
    'click .edit-debate': function(event) {
        var debateId = event.target.getAttribute('debateId');
        //Session.set('debate', Debates.findOne(debateId));
        Session.set('debate', debateId );
        Session.set('includedTemplate', 'DebateMessages');
        Session.set('includedTemplate', 'DebateForm');
        $('#numberParticipantsAlert').css('visibility', 'hidden');
        $('#submitButton').prop('disabled', false);
        var participantsList = [];
        var participants = Debates.findOne(debateId).participants;
        for(var i in participants) {
            participantsList.push(participants[i].participantId);
        }
        $('#debate-select-participants option').each(function() {
            for(var i in participantsList) {
                if($(this).val() == participantsList[i]) {
                    $(this).prop('selected', 'true');
                    participantsList.splice(i, 1);
                    break;
                }
            }
        })
    },
	'click .remove-debate': function(event) {
        var debateId = event.target.getAttribute('debateId');
        $('#confirmation').css('visibility','visible');
        $('#confirmation').css('opacity',1);
        $('#confirmationPopup').attr('value', debateId);
	},
    'click .confirmationButton': function(event) {
        var debateId = $('#confirmationPopup').attr('value');
        if(event.target.id == 'confirmDelete') {
            Meteor.call('removeDebate', debateId);
        }
        $('#confirmation').css('opacity',0);
        setTimeout(function() {
            $('#confirmation').css('visibility','hidden');
        }, 250);
    },
    'click #pause-debate-all': function(event){
        event.preventDefault();
        //pause tous debates
        Debates.find().forEach(function (debate) {
            if(debate.isInProgress())
                Meteor.call('pauseDebate', debate);
        });
        alert("All debate is Paused");
    },
    'click #unpause-debate-all': function(event){
        event.preventDefault();
        //Unpause tous debates
        Debates.find().forEach(function (debate) {
            if(debate.isPaused())
                Meteor.call('unpauseDebate', debate);
        });
        alert("All debate is Unpaused");
    },
});

//Reload each time a dependencie change, we wait for Meteor.user() to be loaded
//    if(Debates) {
//        console.log("oui")
//    } else {
//        console.log("non")
//    }
//    if(Roles.getRolesForUser(Meteor.userId())[0] && !Roles.userIsInRole(Meteor.userId(), ['student'])) {
//        console.log('Redirecting user')
//        var route = '/' + Roles.getRolesForUser(Meteor.userId())[0];
//        FlowRouter.go(route);
//    }
//});