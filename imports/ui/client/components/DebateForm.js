import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating'

import './DebateForm.html';
import { Session } from 'meteor/session'

//Template.InstructorDebatesPage.onCreated(function bodyOnCreated() {
//	var self = this;
//	self.autorun(function() {
//		var courseId = FlowRouter.getParam('courseId');
//		self.subscribe('courseParticipants', courseId);
//		self.subscribe('courses');
//        self.subscribe('users');
//	});
//});
Template.DebateForm.onCreated(function bodyOnCreated() {
    var self = this;
    self.autorun(function () {
        //    self.subscribe('files');
    })
    //delete Session.keys.debateIds;
})
Template.DebateForm.helpers({
    course() {
        return Courses.findOne( { _id : FlowRouter.getParam('courseId') } );
    },
    courseParticipants(debate) {

        return Meteor.users.find(
            {
                roles : "student",
                "profile.courseIds" : FlowRouter.getParam('courseId')
            },
            {
                fields: {
                    "profile.firstName": true,
                    "profile.lastName": true,
                }
            }
        );
    },
    debate() {
        var debateId = Session.get('debate');
        var debate = Debates.findOne({ _id : debateId});

        if(debate) {
            return debate;//Session.get('debate');
        }
    }
//	,
//	selected(participantId) {
//		var debate = Session.get('debate');
//
//		var response = false;
//
//		if(debate) {
//			debate.participants.some(function(element) {
//				if(element.participantId == participantId) {
//					response = true;
//				}
//				return response;
//			});
//		}
//
//		return response;
//	}
});

Template.DebateForm.onRendered(function() {
    $('input[name="startDate"]').datetimepicker();
});


Template.DebateForm.events({
    'submit form': function(event){
        event.preventDefault();

        var form = $(event.target).serializeJSON();

        form.courseId = FlowRouter.getParam('courseId');

        var debateId = Session.get('debate');
        var debate = Debates.findOne({ _id : debateId});

        // Replacing stringDate with timestamp
        form.startDate = new Date(form.startDate).getTime();

        var files = event.target.fileUpload.files;
        //console.log("The files receive is " + files);
        if(debate) {
            Meteor.call('updateDebate', debate._id, form, function(error, result) {
                if(!error) {
                    //console.log("Debate updated's id is " + result);

                    Session.set('debate', undefined);
                    delete Session.keys.debate;

                    Session.set('debateIds', result);

                    event.target.reset();
                }
            });
        }
        else {
            Meteor.call('addDebate', form, function(error, result) {
                if(!error) {

                    Session.set('debate', undefined);
                    delete Session.keys.debate;

                    Session.set('debateIds', result);

                    event.target.reset();
                }
            }) ;
        }
        //console.log("Debate inserted's id is " + Session.get('debateIds'));

        if(files){
            for (var i = 0, ln = files.length; i < ln; i++) {
                FileCollection.insert(files[i], function (err, fileObj) {
                    if(err){
                        console.log("callback for the insert, err: ", err);
                    }
                    else {
                        var productFile = "/cfs/files/fileCollection/" + fileObj._id;
                        console.log("The url is " + productFile);
                        console.log("The name is " + fileObj.name());
                        FileFS.insert({
                            url: productFile,
                            debateId: Session.get('debateIds'),
                            name: fileObj.name()
                        });
                    }
                });
            };

            //delete Session.keys.debateIds;
        }
    },

    "input #studentSearch": function(event){
        studentSearch = new RegExp($(event.target).val(), 'i');

        $.map($('#debate-select-participants option') ,function(option){
            if(option.value != ""){
                var firstNameLastName = $(option).attr("studentFirstName") + " " + $(option).attr("studentLastName");
                var lastNameFirstName = $(option).attr("studentLastName") + " " + $(option).attr("studentFirstName");

                if($(option).attr("studentFirstName").search(studentSearch) != -1 || $(option).attr("studentLastName").search(studentSearch) != -1 || firstNameLastName.search(studentSearch) != -1 || lastNameFirstName.search(studentSearch) != -1){
                    $(option).show();
                } else {
                    $(option).hide();
                }
            }

        })
    },

    'change #fileUpload': function (event) {
        event.preventDefault();
        /*var fileFS = FileFS.find({
         debateId: Session.get('debateIds')
         }).map(function (doc) {
         console.log("The file url in last inserted in collectio FS " + doc.url );
         console.log(" The file name is " + doc.name);
         })*/
        // var files = $('#fileUpload').target.files;
        //console.log(files);
        /*for (var i = 0, ln = files.length; i < ln; i++) {
         FileCollection.insert(files[i], function (err, fileObj) {
         console.log("callback for the insert, err: ", err);
         var productFile = fileObj.url();
         console.log("File insert success ", productFile);
         FileFS.insert({
         url: productFile,
         courseId: FlowRouter.getParam('courseId')
         });
         if (!err) {
         console.log("inserted without error" +productFile);
         delete Session.keys.courseId;
         delete Session.keys.file;
         }
         else {
         console.log("there was an error", err);
         }
         });
         };
         /*FS.Utility.eachFile(event, function(file) {
         FileCollection.insert(file, function (err, fileObj) {
         // Inserted new doc with ID fileObj._id, and kicked off the data upload using HTTP
         //console.log("callback for the insert, err: ", err);
         FileFS.insert({
         url: productFile,
         courseId: FlowRouter.getParam('courseId')
         });
         if (!err) {
         var productFile = fileObj.uploadedAt();
         console.log("File insert success ", productFile);
         }

         else {
         console.log("there was an error", err);
         }
         });
         });*/
    },
    // Allow multiple selection without maintaining Ctrl key. Also, scroll back to position before selection
    "mousedown": function (e) {

        var el = e.target;
        if (el.tagName.toLowerCase() == 'option' && el.parentNode.hasAttribute('multiple')) {
            e.preventDefault();

            var scroll = $('#debate-select-participants').scrollTop();

            e.target.selected = !e.target.selected;

            setTimeout(function() {
                $('#debate-select-participants').scrollTop(scroll);
            }, 0);

            //Check if the number of participants is equal to 8, 12 or 16

            $('#submitButton').prop('disabled', false);
            $('#numberParticipantsAlert').css('visibility', 'hidden');

//        if($('#debate-select-participants').find(':selected').length % 4 == 0 && $('#debate-select-participants').find(':selected').length > 4) {
//            $('#submitButton').prop('disabled', false);
//            $('#numberParticipantsAlert').css('visibility', 'hidden');
//        } else {
//            $('#submitButton').prop('disabled', true);
//            $('#numberParticipantsAlert').css('visibility', 'visible');
//        }
        }
    }
});