import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating'

import './DebateMessages.html';
import '/imports/api/Debates/debates.js'

Template.DebateMessages.helpers({
	debateInterventions() {
		var debateId = Session.get('debate');
        var debate = Debates.findOne({ _id : debateId});

        if(debate) {
			var debateInterventions = new Mongo.Collection(null);
			for(var i in debate.participants) {
				var participant = debate.participants[i];
				for(var j in participant.interventions) {
					debateInterventions.insert({
						message: participant.interventions[j].text,
						messageDate: participant.interventions[j].date,
						teamNumber: participant.teamNumber,
						author: Meteor.users.findOne(participant.participantId).profile
					});
				}
			}

            for(var i in debate.instructorInterventions) {
                var intervention = debate.instructorInterventions[i];
                debateInterventions.insert({
                    message: intervention.text,
                    messageDate: intervention.date,
                    teamNumber: 10,
                    author: Meteor.users.findOne(intervention.instructorId).profile
                })
            }

			return debateInterventions.find({}, {sort: {messageDate: 1}});
		}
	},
	debateDocument() {
		var debateId = Session.get('debate');
        var debate = Debates.findOne({ _id : debateId});

        if(debate) {
			var debateDocument = Debates.findOne(debate._id);
			return debateDocument;
		}
	}
});

Template.DebateMessages.events({
	'click #pause-debate': function(event){
		event.preventDefault();

		var debateId = Session.get('debate');
        var debate = Debates.findOne({ _id : debateId});

        Meteor.call('pauseDebate', debate);
	},
	'click #unpause-debate': function(event){
		event.preventDefault();

		var debateId = Session.get('debate');
        var debate = Debates.findOne({ _id : debateId});

        Meteor.call('unpauseDebate', debate);
	},
    'keypress #add-time-debate': function (event) {
        var debateId = Session.get('debate');
        var debate = Debates.findOne({ _id : debateId});

        if (event.keyCode == 13){
            event.preventDefault();
            var time = parseInt(event.target.value);
            var debateRound = debate.round().number;
            Meteor.call('addTimeDebate', debate, time, debateRound);
            event.target.value = '';
        }
    },
    'input #instructorIntervention': function(event) {
        if(event.target.value == '') {
            $('#submitInstructorIntervention').prop('disabled', true);
        } else {
            $('#submitInstructorIntervention').prop('disabled', false);
        }
    },
    /*'click #pause-debate-all': function(event){
		event.preventDefault();
		//pause tous debates
        Debates.find().forEach(function (debate) {
            Meteor.call('pauseDebate', debate);
        });
        alert("All debate is Paused");
       },
    'click #unpause-debate-all': function(event){
        event.preventDefault();
        //Unpause tous debates
        Debates.find().forEach(function (debate) {
            Meteor.call('unpauseDebate', debate);
        });
        alert("All debate is Unpaused");
    },*/
    'submit #instructorInterventionForm': function(event) {
        event.preventDefault();
        
//        console.log(Session.get('debate')._id, $('#instructorIntervention').val())
        
        var message = $('#instructorIntervention').val();
        
        Meteor.call('postInstructorMessage', Session.get('debate'), message, function(error, result) {
			console.log(error);
//			if(error) {
//				$('textarea[name="message"]').parent().addClass('has-error');
//				if(error.error = 'not-authorized') {
//					console.log(error.error);
//					$('#textarea-message-error').html('You can\'t participate in debate.');
//				}
//				else if(error.error == 'debate-finished') {
//					console.log(error.error);
//					$('#textarea-message-error').html('Debate is finished.');
//				}
//				else if(error.error == 'debate-not-begun') {
//					console.log(error.error);
//					$('#textarea-message-error').html('Debate has not begun.');
//				}
//				else if(error.error == 'not-speaker-turn') {
//					console.log(error.error);
//					$('#textarea-message-error').html('It is not user turn to speak.');
//				}
//				else if(error.error == 'already-spoke') {
//					console.log(error.error);
//					$('#textarea-message-error').html('You already spoke this round.');
//				}
//				else if(error.error == 'message-length') {
//					console.log(error.error);
//					$('#textarea-message-error').html('Your message is too ' + error.reason + '.');
//				}
//			}
//			else {
//				$('textarea[name="message"]').parent().removeClass('has-error');
//				$('#textarea-message-error').html('');
//				$('#message-length').html('');
//				event.target.reset();
//			}
		});
        
        // Reset textarea value
        $('#instructorIntervention').val('');
        $('#submitInstructorIntervention').prop('disabled', true);
    }
});