/**
 * Created by TranUyDu on 15/05/2017.
 */
import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { Tracker } from 'meteor/tracker';

import './methods.js';
CoursesAd = new Mongo.Collection('coursesAd');

CoursesAd.schema = new SimpleSchema({
    name: {
        type: String,
        label: "CoursesAd 's name",
        unique: true
    }
})

CoursesAd.attachSchema(CoursesAd.schema);

CoursesAd.helpers({
    school() {
        return Schools.findOne(
            {
                _id: this.schoolId
            }
        );
    }
});