/**
 * Created by TranUyDu on 15/05/2017.
 */
// Courses.helpers({
// 	school() {
// 		return Schools.find({_id: this.school}, {sort: {name: 1}});
// 	}
// });

Meteor.methods({
    addCourseAd: function (courseObject) {
        // Retrieve informations about the course
        console.log("The form object is " + courseObject.name);

        // Make sure the user is logged in before inserting a task
        if(!this.userId || !Roles.userIsInRole(this.userId, ['admin'])) {
            throw new Meteor.Error('not-authorized');
        }

        check(courseObject, CoursesAd.schema);

        // Add course to the collection
        CoursesAd.insert(courseObject, function(error, result){
            return result;
        });
    }
});