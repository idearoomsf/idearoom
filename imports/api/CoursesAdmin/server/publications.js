/**
 * Created by TranUyDu on 15/05/2017.
 */
Meteor.publish('coursesAd', function coursesPublication() {
    if (Roles.userIsInRole(this.userId, ['admin'])) {
        return CoursesAd.find();
    }
    else if (Roles.userIsInRole(this.userId, ['student'])) {
        var courseIds = Meteor.users.findOne({_id: this.userId}, {fields: {_id: 0, 'profile.courseIds': 1}}).profile.courseIds;
        return CoursesAd.find({
            _id : { $in: courseIds }
        });
    }
    else {
        // user not authorized. do not publish secrets
        this.stop();
        return;
    }
});