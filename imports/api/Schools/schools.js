import { Mongo } from 'meteor/mongo';
import { SimpleSchema } from 'meteor/aldeed:simple-schema';

import './methods.js';

Schools = new Mongo.Collection('schools');

Schools.allow(
	{
		insert: function() {
			return true;
		}
	}
)

Schools.schema = new SimpleSchema({
	name: {
		type: String,
		label: "Name",
		unique: true
	}
});

Schools.attachSchema(Schools.schema);

Schools.helpers({
	courses() {
		return Courses.find({schoolId: this._id});
	},
	coursesCount() {
		return Courses.find({schoolId: this._id}, {fields:{name:1}}).count();
	}
});