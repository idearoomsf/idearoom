import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { Tracker } from 'meteor/tracker';

import './methods.js';

Debates = new Mongo.Collection('debates');

Debates.schema = new SimpleSchema({
    title: {
        type: String,
        label: "Title"
    },
    description: {
        type: String,
        label: "Description"
    },
    courseId: {
        type: String,
        label: "Course",
        // regex: SimpleSchema.RegEx.Id
    },
    timeAdd:{
        type: [Object],
        label: "Debate's Add time",
        optional: true
    },
    "timeAdd.$.value":{
        type: Number,
        label: "Debate's time added",
        defaultValue: 0
    },
    "timeAdd.$.turn":{
        type: Number,
        label: "Participant's intervention turn",
        optional: true,
        defaultValue: 0
    },
    /*timeAdd: {
        type: Number,
        label: "Debate's time added",
        defaultValue: 0
    },*/
    instructorInterventions: {
        type: [Object],
        label: "Instructor's interventions",
        optional: true
    },
    "instructorInterventions.$.text": {
        type: String,
        label: "Intervention's text",
        optional: true
    },
    "instructorInterventions.$.date": {
        type: Date,
        label: "Intervention's date",
        optional: true
    },
    "instructorInterventions.$.instructorId": {
        type: String,
        label: "Instructor's id",
        optional: true
    },
    participants: {
        type: [Object],
        label: "Participants",
        minCount: 4,
//		maxCount: 50,
        // regex: SimpleSchema.RegEx.Id
    },
    "participants.$.participantId": {
        type: String,
        label: "Participant's id"
    },
    "participants.$.speaker": {
        type: Boolean,
        label: "Speaker's role'"
    },
    "participants.$.subTeamNumber":{
        type: Number,
        label: "Subteam Number"
    },
    "participants.$.teamNumber": {
        type: Number,
        label: "Participant's team number"
    },
    "participants.$.order": {
        type: Number,
        label: "Participant's order in debate"
    },
    "participants.$.interventions": {
        type: [Object],
        label: "Interventions list"
    },
    "participants.$.interventions.$.text": {
        type: String,
        label: "Intervention's text",
        optional: true
    },
    "participants.$.interventions.$.date": {
        type: Date,
        label: "Intervention's date",
        optional: true
    },
    "participants.$.interventions.$.round": {
        type: Number,
        label: "Intervention's round",
        optional: true
    },
    chatLive: {
        type: [Object],
        label: "Chat live",
        optional: true
    },
    "chatLive.$.team": {
        type: Number,
        label: "Team's number"
    },
    "chatLive.$.subteam": {
        type: Number,
        label: "Subteam's number"
    },
    "chatLive.$.destination": {
        type: String,
        label: "Message goes to team or subteam"
    },
    "chatLive.$.message": {
        type: String,
        label: "Chat message"
    },
    "chatLive.$.date": {
        type: Date,
        label: "Chat message's date"
    },
    "chatLive.$.author": {
        type: String,
        label: "author's Id"
    },
    "chatLive.$.isCurrentUser": {
        type: Boolean,
        label: "Is the author of the message the current user",
        optional: true
    },
//    chatLiveTeam1SubTeam1: {
//        type: [Object],
//        label: "Chat live team 1 subteam 1",
//        optional: true
//    },
//    "chatLiveTeam1SubTeam1.$.message": {
//        type: String,
//        label: "Chat message team 1 subteam 1"
//    },
//    "chatLiveTeam1SubTeam1.$.date": {
//        type: Date,
//        label: "Chat message's date team 1 subteam 1"
//    },
//    "chatLiveTeam1SubTeam1.$.author": {
//        type: String,
//        label: "author's Id team 1 subteam 1"
//    },
//    "chatLiveTeam1SubTeam1.$.isCurrentUser": {
//        type: Boolean,
//        label: "Is the author of the message the current user",
//        optional: true
//    },
//    chatLiveTeam1SubTeam2: {
//        type: [Object],
//        label: "Chat live team 1 subteam 2",
//        optional: true
//    },
//    "chatLiveTeam1SubTeam2.$.message": {
//        type: String,
//        label: "Chat message team 1 subteam 2"
//    },
//    "chatLiveTeam1SubTeam2.$.date": {
//        type: Date,
//        label: "Chat message's date team 1 subteam 2"
//    },
//    "chatLiveTeam1SubTeam2.$.author": {
//        type: String,
//        label: "author's Id team 1 subteam 2"
//    },
//    "chatLiveTeam1SubTeam2.$.isCurrentUser": {
//        type: Boolean,
//        label: "Is the author of the message the current user",
//        optional: true
//    },
//    chatLiveTeam2SubTeam1: {
//        type: [Object],
//        label: "Chat live team 2 subteam 1",
//        optional: true
//    },
//    "chatLiveTeam2SubTeam1.$.message": {
//        type: String,
//        label: "Chat message team 2 subteam 1"
//    },
//    "chatLiveTeam2SubTeam1.$.date": {
//        type: Date,
//        label: "Chat message's date team 2 subteam 1"
//    },
//    "chatLiveTeam2SubTeam1.$.author": {
//        type: String,
//        label: "author's Id team 2 subteam 1"
//    },
//    "chatLiveTeam2SubTeam1.$.isCurrentUser": {
//        type: Boolean,
//        label: "Is the author of the message the current user",
//        optional: true
//    },
//    chatLiveTeam2SubTeam2: {
//        type: [Object],
//        label: "Chat live team 2 subteam 2",
//        optional: true
//    },
//    "chatLiveTeam2SubTeam2.$.message": {
//        type: String,
//        label: "Chat message team 2 subteam 2"
//    },
//    "chatLiveTeam2SubTeam2.$.date": {
//        type: Date,
//        label: "Chat message's date team 2 subteam 2"
//    },
//    "chatLiveTeam2SubTeam2.$.author": {
//        type: String,
//        label: "author's Id team 2 subteam 2"
//    },
//    "chatLiveTeam2SubTeam2.$.isCurrentUser": {
//        type: Boolean,
//        label: "Is the author of the message the current user",
//        optional: true
//    },
    startDate: {
        type: Date,
        label: "Start date of debate"
    },
    roundDurationPhase1: {
        type: Number,
        label: "Duration of rounds during first phase"
    },
    roundDurationPhase2: {
        type: Number,
        label: "Duration of rounds during second phase"
    },
    interventionMinLengthPhase1: {
        type: Number,
        label: "Minimum length of interventions during first phase"
    },
    interventionMaxLengthPhase1: {
        type: Number,
        label: "Maximum length of interventions during first phase"
    },
    interventionMinLengthPhase2: {
        type: Number,
        label: "Minimum length of interventions during second phase"
    },
    interventionMaxLengthPhase2: {
        type: Number,
        label: "Maximum length of interventions during second phase"
    },
    pauses: {
        type: [Object],
        label: "Debate's pauses",
        optional: true
    },
    "pauses.$.startDate": {
        type: Date,
        label: "Pause's start date"
    },
    "pauses.$.endDate": {
        type: Date,
        label: "Pause's end date",
        optional: true
    },
    linkResources:{
        type: String,
        label: "Resource Link used fo Debate",
        optional: true
    }
});

Debates.attachSchema(Debates.schema);

Debates.helpers({
    isStarted() {
        return this.startDate < new Date();
    },
    endDate() {
        if(this.isPaused()) {
            return false;
        }

        var timeAdd = 0 ;

        for(var i in this.timeAdd){
            timeAdd += this.timeAdd[i].value ;
        }
            

        // Durée en minutes de toutes les interventions
        var debateInterventionsDuration = (4 * (this.roundDurationPhase1 + this.roundDurationPhase2)) + timeAdd;

        // Durée en minutes des pauses
        var debatePausesDuration = 0;
        for(var j in this.pauses) {
            debatePausesDuration += ((this.pauses[j].endDate - this.pauses[j].startDate)/60000);
        }

        var endDate = new Date(this.startDate.getTime());
        endDate.setMinutes((endDate.getMinutes() + debateInterventionsDuration + debatePausesDuration));

        return endDate;
    },
    isFinished() {
        return this.endDate() < new Date();
    },
    isInProgress() {
        return this.isStarted() && !this.isFinished();
    },
    isPaused() {
        if(this.pauses && this.pauses.length > 0) {
            return !this.pauses[this.pauses.length - 1].endDate;
        }
        else {
            return false;
        }
    },
    isAllPaused() {
        Debates.find().forEach(function (debate) {
        })
    },
    round() {
        var now = new Date();
        var timeAdded = 0 ;
        var minutesDiff = (now - this.startDate)/(60000);

        var pausesLength = 0;
        for(i in this.pauses) {
            if(this.pauses[i].endDate) {
                pausesLength += ((this.pauses[i].endDate - this.pauses[i].startDate)/60000);
            }
            else {
                pausesLength += ((now - this.pauses[i].startDate)/60000);
            }
        }

        for(var i in this.timeAdd) {
            timeAdded += this.timeAdd[i].value ;
        }

        debatesLength = minutesDiff - pausesLength - timeAdded;
        var roundEndDate = new Date(this.startDate.getTime());

         var roundNumber = 0;
         var temps = 0;
         var temps2 = 0;
        while (roundEndDate < now){
            if(roundNumber < 4){
                temps = roundEndDate.getMinutes();
                temps += this.roundDurationPhase1;
                roundEndDate.setMinutes(temps);
            }
            else {
                temps2 = roundEndDate.getMinutes();
                temps2 += this.roundDurationPhase1;

                roundEndDate.setMinutes(temps2);
            }
            for(var j in this.timeAdd) {
                if(roundNumber == (this.timeAdd[j].turn - 1)) {

                    roundEndDate.setMinutes((roundEndDate.getMinutes()  + this.timeAdd[j].value));
                }
            }
            roundNumber++;
        }

        return {'number': roundNumber, 'endDate': roundEndDate};
    },


    phase() {
        var round = this.round().number;
        if(round <= 4) {
            return {'name': 'Phase argumentative', 'round': round};
        }
        else {
            return {'name': 'Phase de reformulation', 'round': round};
        }
    },
    interventionLengths(round) {
        if(round <= 4) {
            return {'min': this.interventionMinLengthPhase1, 'max': this.interventionMaxLengthPhase1};
        }
        else {
            return {'min': this.interventionMinLengthPhase2, 'max': this.interventionMaxLengthPhase2};
        }
    },
    minLength(round) {
        if(round <= 4) {
            return this.interventionMinLengthPhase1;
        }
        else {
            return this.interventionMinLengthPhase2;
        }
    },
    maxLength(round) {
        if(round <= 4) {
            return this.interventionMaxLengthPhase1;
        }
        else {
            return this.interventionMaxLengthPhase2;
        }
    },
    speakerOrder(round) {
        if(round <= 4) {
            return round;
        }
        else {
            switch(round) {
                case 5:
                    return 2;

                case 6:
                    return 1;

                case 7:
                    return 4;

                case 8:
                    return 3;
            }
        }
    },
    speaker(round) {
        var speakerOrder = this.speakerOrder(round);

        for(i in this.participants) {
            if(this.participants[i].order == speakerOrder && this.participants[i].speaker == true) {
                var speaker = this.participants[i]
                break;
            }
        }

        var speakerUser = Meteor.users.findOne(speaker.participantId);

        speaker.profile = speakerUser ? speakerUser.profile : null;

        return speaker;
    },
    viewer(round){
        var speakerOrder = this.speakerOrder(round);

        var currentTeam = 0;
        var currentSubTeam = 0;

        for(i in this.participants) {
            if(this.participants[i].order == speakerOrder) {
                currentTeam = this.participants[i].teamNumber;
                currentSubTeam = this.participants[i].subTeamNumber;
                break;
            }
        }

        var viewersList = [];

        for(i in this.participants){
            if(this.participants[i].teamNumber == currentTeam && this.participants[i].subTeamNumber == currentSubTeam && this.participants[i].speaker == false){
                viewer = this.participants[i];
                viewerDocument = Meteor.users.findOne(viewer.participantId);
                if(viewerDocument) {
                    viewersList.push(viewerDocument.profile);
                }
            }
        }
        return viewersList;
    },
    isParticipant(userId) {
        for(var i in this.participants) {
            if(this.participants[i].participantId == userId) {
                return true;
            }
        }

        return false;
    },
    isCurrentUserParticipant() {
        return this.isParticipant(Meteor.userId());
    },
    firstSpeaker() {
        return this.speaker(1);
    },
    currentSpeaker() {
        return this.speaker(this.round().number);
    },
    participantsProfile() {
        participantsProfile = [];
        for(var i in this.participants) {
            var participant = this.participants[i];
            var participantDocument = Meteor.users.findOne(participant.participantId);
            if(participantDocument) {
                participantsProfile[participant.order] = participantDocument.profile;
            }
        }
        return participantsProfile;
    },
    participantsProfileByTeam() {

        participantsProfile = {
            1: [],
            2: []
        };

        for(var i in this.participants) {
            var participant = this.participants[i];
            var user = Meteor.users.findOne(participant.participantId);
            if(user) {
                participantsProfile[participant.teamNumber].push(user.profile);
            }

//        for(var i in this.participants) {
//            var participant = this.participants[i];
//            var user = Meteor.users.findOne(participant.participantId);
//            if(user) {
//                console.log(user)
//                participantsProfile[participant.teamNumber][participant.order] = user.profile;
//            }
//            participantsProfile[participant.teamNumber][participant.order] = Meteor.users.findOne(participant.participantId).profile;
        }

        return participantsProfile;
    }
});