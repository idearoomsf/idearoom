Meteor.methods({
    addDebate: function (debateObject) {
        // Make sure the user is logged in before inserting a task
        if (!this.userId || !Roles.userIsInRole(this.userId, ['instructor'])) {
            throw new Meteor.Error('not-authorized');
        }

        var participantList = debateObject.participantList;
        delete debateObject.participantList;

        // Shuffle the participantList
        for (var i = participantList.length - 1; i > 0; i--) {
            var j = Math.floor(Math.random() * (i + 1));
            var temp = participantList[i];
            participantList[i] = participantList[j];
            participantList[j] = temp;
        }

        debateObject.participants = [];

        for (var k = 0; k < participantList.length; k++) {
            debateObject.participants.push(
                {
                    participantId: participantList[k],
                    teamNumber: k % 2 == 0 ? 1 : 2,
                    order: k + 1,
                    interventions: [],
                    speaker: false,
                    subTeamNumber: Math.floor((k % 4) / 2) + 1
                }
            );

            if (k < 4) {
                debateObject.participants[k].speaker = true;
            }
        }

//        Meteor.call('updateDebateRole', debateObject.participants);

        debateObject.roundDurationPhase1 = parseInt(debateObject.roundDurationPhase1);
        debateObject.roundDurationPhase2 = parseInt(debateObject.roundDurationPhase2);
        debateObject.interventionMinLengthPhase1 = parseInt(debateObject.interventionMinLengthPhase1);
        debateObject.interventionMaxLengthPhase1 = parseInt(debateObject.interventionMaxLengthPhase1);
        debateObject.interventionMinLengthPhase2 = parseInt(debateObject.interventionMinLengthPhase2);
        debateObject.interventionMaxLengthPhase2 = parseInt(debateObject.interventionMaxLengthPhase2);

        debateObject.startDate = new Date(debateObject.startDate);

        check(debateObject, Debates.schema);

        Debates.insert(debateObject);

        var debate = Debates.findOne(debateObject);
        return debate._id;

    },
    // TRAKTODO update function
    updateDebate: function (debateId, debateObject) {
        // Make sure the user is logged in before inserting a task
        if (!this.userId || !Roles.userIsInRole(this.userId, ['instructor'])) {
            throw new Meteor.Error('not-authorized');
        }

        var participantList = debateObject.participantList;
        var participantListBackup = participantList;
        delete debateObject.participantList;

        // Shuffle the participantList
        for (var i = participantList.length - 1; i > 0; i--) {
            var j = Math.floor(Math.random() * (i + 1));
            var temp = participantList[i];
            participantList[i] = participantList[j];
            participantList[j] = temp;
        }

        debateObject.participants = [];

        for (var k = 0; k < participantList.length; k++) {
            debateObject.participants.push(
                {
                    participantId: participantList[k],
                    teamNumber: k % 2 == 0 ? 1 : 2,
                    order: k + 1,
                    interventions: [],
                    speaker: false,
                    subTeamNumber: 0
                }
            );
            for (var i = 0; i < participantListBackup.length; i++) {
                if (participantListBackup[i].participantId == participantList[k]) {
                    debateObject.participants.speaker = participantListBackup[i].speaker;
                    debateObject.participants.subTeamNumber = participantListBackup[i].subTeamNumber;
                }
            }
            ;
        }


//		for (var i = participantList.length - 1; i > 0; i--) {
//	        var j = Math.floor(Math.random() * (i + 1));
//	        var temp = participantList[i];
//	        participantList[i] = participantList[j];
//	        participantList[j] = temp;
//	    }
//
//	    debateObject.participants = [];
//
//		for(var k = 0; k < participantList.length; k++) {
//			debateObject.participants.push(
//				{
//					participantId: participantList[k],
//					teamNumber: k % 2 == 0 ? 1 : 2,
//					order: k+1,
//					interventions: [],
//                    speaker: false,
//                    subTeamNumber: 0
//				}
//			);
//            for(var i = 0; i < participantListBackup.length; i++){
//                if(participantListBackup[i].participantId == participantList[k]){
//                    debateObject.participants.speaker = participantListBackup[i].speaker;
//                    debateObject.participants.subTeamNumber = participantListBackup[i].subTeamNumber;
//                }
//            }
//		}

        debateObject.roundDurationPhase1 = parseInt(debateObject.roundDurationPhase1);
        debateObject.roundDurationPhase2 = parseInt(debateObject.roundDurationPhase2);
        debateObject.interventionMinLengthPhase1 = parseInt(debateObject.interventionMinLengthPhase1);
        debateObject.interventionMaxLengthPhase1 = parseInt(debateObject.interventionMaxLengthPhase1);
        debateObject.interventionMinLengthPhase2 = parseInt(debateObject.interventionMinLengthPhase2);
        debateObject.interventionMaxLengthPhase2 = parseInt(debateObject.interventionMaxLengthPhase2);

        debateObject.startDate = new Date(debateObject.startDate);

//		var endDate = new Date(debateObject.startDate.getTime());
//
//		endDate.setDate(endDate.getDate() + participantList.length);
//
//		debateObject.endDate = endDate;

        check(debateObject, Debates.schema);

        Debates.update({_id: debateId}, {$set: debateObject});

        return Debates.findOne(debateObject);
    },

    removeDebate: function (debateId) {
        // Make sure the user is logged in before inserting a task
        if (!this.userId || !Roles.userIsInRole(this.userId, ['instructor'])) {
            throw new Meteor.Error('not-authorized');
        }

        Debates.remove(debateId);
    },

    postMessage: function (debateId, message, autoPublication) {
        // Make sure the user is logged in before inserting a task
        if (!this.userId) {
            if (Meteor.isServer) {
                throw new Meteor.Error('not-authorized', 'user undefined');
            }
        }

        var debate = Debates.findOne(debateId);

        for (var i in debate.participants) {
            if (debate.participants[i].participantId == this.userId) {
                var userIndex = i;
                var userOrder = debate.participants[i].order;
            }
        }

        if (!userIndex) {
            if (Meteor.isServer) {
                throw new Meteor.Error('not-authorized', 'user not in participant\'s list');
            }
        }

        var now = new Date();

        if (now > debate.endDate()) {
            if (Meteor.isServer) {
                throw new Meteor.Error('debate-finished');
            }
        }

        if (now < debate.startDate) {
            if (Meteor.isServer) {
                throw new Meteor.Error('debate-not-begun');
            }
        }

        var round = debate.round();
        var currentSpeaker = debate.speaker(round.number);

        if (currentSpeaker.participantId != this.userId) {
            if (Meteor.isServer) {
                throw new Meteor.Error('not-speaker-turn');
            }
        }

        for (var i in currentSpeaker.interventions) {
            if (currentSpeaker.interventions[i].round == round.number) {
                if (Meteor.isServer) {
                    throw new Meteor.Error('already-spoke');
                }
            }
        }

        if (!autoPublication) {
            var messageWithoutSpaces = message.replace(/\s/g, "");

            var interventionLengths = debate.interventionLengths(round.number);
            if (messageWithoutSpaces.length < interventionLengths.min || messageWithoutSpaces.length > interventionLengths.max) {
                if (Meteor.isServer) {
                    throw new Meteor.Error('message-length', messageWithoutSpaces.length < interventionLengths.min ? 'short' : 'long');
                }
            }
        }

        if (message) {
            check(message, String);

            Debates.update({
                    _id: debateId,
                    "participants.participantId": this.userId
                },
                {
                    $push: {
                        "participants.$.interventions": {
                            text: message,
                            date: new Date(),
                            round: round.number
                        }
                    }
                });
        }
    },
    postInstructorMessage: function (debateId, message) {
        // Make sure the user is logged in before inserting a task
        if (!this.userId) {
            if (Meteor.isServer) {
                throw new Meteor.Error('not-authorized', 'user undefined');
            }
        }

        var debate = Debates.findOne(debateId);

        var now = new Date();

        if (now > debate.endDate()) {
            if (Meteor.isServer) {
                throw new Meteor.Error('debate-finished');
            }
        }

        if (now < debate.startDate) {
            if (Meteor.isServer) {
                throw new Meteor.Error('debate-not-begun');
            }
        }

        check(message, String);

        Debates.update({
                _id: debateId
            },
            {
                $push: {
                    "instructorInterventions": {
                        text: message,
                        date: new Date(),
                        instructorId: this.userId
                    }
                }
            });
    },

    postChat: function (debateId, message, team, subteam, destination) {
        // Make sure the user is logged in before inserting a task
        if (!this.userId) {
            if (Meteor.isServer) {
                throw new Meteor.Error('not-authorized', 'user undefined');
            }
        }

        var debate = Debates.findOne(debateId);

        var now = new Date();

        if (now > debate.endDate()) {
            if (Meteor.isServer) {
                throw new Meteor.Error('debate-finished');
            }
        }

        if (now < debate.startDate) {
            if (Meteor.isServer) {
                throw new Meteor.Error('debate-not-begun');
            }
        }

        check(message, String);

        var pushRequest = {$push: {}};
        pushRequest.$push['chatLive'] = {
            message: message,
            date: new Date(),
            author: this.userId,
            team: team,
            subteam: subteam,
            destination: destination
        };

        Debates.update({_id: debateId}, pushRequest);

//		Debates.update({
//			_id: debateId
//		},
//		{
//			$push: {
//				'chatLiveTeam' + 1 + 'SubTeam1': {
//					message: message,
//					date: new Date(),
//					author: this.userId
//				}
//			}
//		});
    },

    pauseDebate: function (debate) {
        var debateObject = Debates.findOne(debate._id);

        if (debateObject.isPaused()) {
            if (Meteor.isServer) {
                throw new Meteor.Error('already-paused');
            }
        }


        Debates.update({
                _id: debateObject._id,
            },
            {
                $push: {
                    pauses: {
                        startDate: new Date()
                    }
                }
            });
    },
    unpauseDebate: function (debate) {
        var debateObject = Debates.findOne(debate._id);

        if (!debateObject.isPaused()) {
            if (Meteor.isServer) {
                throw new Meteor.Error('is-not-paused');
            }
        }

        var pauses = debateObject.pauses;

        for (var i in pauses) {
            if (!pauses[i].endDate) {
                pauses[i].endDate = new Date();
            }
        }

        Debates.update({
                _id: debateObject._id,
            },
            {
                $set: {
                    pauses: pauses
                }
            });
    },
    addTimeDebate: function (debate ,time,round) {
        var debateObject = Debates.findOne(debate._id);
        var timeAdded = time;

        Debates.update({
                _id: debateObject._id,
            },
            {
                $push: {
                    timeAdd: {
                        value: timeAdded,
                        turn: parseInt(round)
                    }
                }
            }
        );
    },
});