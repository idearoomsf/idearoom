Meteor.publish('debates', function debatesPublication() {
	if (Roles.userIsInRole(this.userId, ['admin'])) {
		return Debates.find();
	}
	else if (Roles.userIsInRole(this.userId, ['instructor', 'student'])) {
		var courseIds = Meteor.users.findOne({_id: this.userId}, {fields: {_id: 0, 'profile.courseIds': 1}}).profile.courseIds;
		return Debates.find({
			courseId : { $in: courseIds }
		});
	}
	else {
		// user not authorized. do not publish secrets
		this.stop();
		return;
	}
});