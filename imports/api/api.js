import './Users/server/users.js';
import './Users/methods.js';

import './Courses/courses.js';
import './Courses/server/publications.js';


import './CoursesAdmin/coursesAd.js';
import './CoursesAdmin/server/publications.js';


import './Debates/debates.js';
import './Debates/server/publications.js';

import './Schools/schools.js';
import './Schools/server/publications.js';

import './Files/files.js';
import './Files/image.js';
import './Files/server/publication.js';
