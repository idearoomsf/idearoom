import { Accounts } from 'meteor/accounts-base';
import { Roles } from 'meteor/alanning:roles';

import './setRolesOnUserObj.js';

Accounts.onCreateUser(function(options, user) {
    if (options.profile.role)
        Roles.setRolesOnUserObj(user, [options.profile.role]);

    return user;
});

Schema = {};

Schema.UserProfile = new SimpleSchema({
    firstName: {
        type: String,
        optional: true
    },
    lastName: {
        type: String,
        optional: true
    },
    schoolId: {
        type: String,
        optional: true
    },
    courseIds: {
        type: [String],
        optional: true
    }
});

Schema.User = new SimpleSchema({
    emails: {
        type: Array,
        // For accounts-password, either emails or username is required, but not both. It is OK to make this
        // optional here because the accounts-password package does its own validation.
        // Third-party login packages may not require either. Adjust this schema as necessary for your usage.
        optional: true
    },
    "emails.$": {
        type: Object
    },
    "emails.$.address": {
        type: String,
        regEx: SimpleSchema.RegEx.Email
    },
    "emails.$.verified": {
        type: Boolean
    },
    createdAt: {
        type: Date
    },
    profile: {
        type: Schema.UserProfile,
        optional: true
    },
    // Make sure this services field is in your schema if you're using any of the accounts packages
    services: {
        type: Object,
        optional: true,
        blackbox: true
    },
    // Option 2: [String] type
    // If you are sure you will never need to use role groups, then
    // you can specify [String] as the type
    roles: {
        type: [String],
        optional: true
    },
    // In order to avoid an 'Exception in setInterval callback' from Meteor
    heartbeat: {
        type: Date,
        optional: true
    }
});

Meteor.users.attachSchema(Schema.User);


Meteor.publish('courseParticipants', function courseParticipantsPublication(courseId) {
	return Meteor.users.find(
		{
			"profile.courseIds": courseId
		}
	);
});

// TRAKTODO remake publications to publish only debate participants
Meteor.publish('courseParticipantsFromDebate', function courseParticipantsFromDebatePublication(debateId) {
    var debate = Debates.findOne(debateId);
    if(debate.isParticipant(this.userId)) {
        return Meteor.users.find(
            {
                "profile.courseIds": debate.courseId,
//                roles: "student"
            }
        );
    }
    else {
        return null;
    }
        
});

Meteor.publish('users', function usersPublication() {
	if (Roles.userIsInRole(this.userId, ['admin'])) {
		return Meteor.users.find();
	}
	else {
		// user not authorized. do not publish secrets
		this.stop();
		return;
	}
});

Meteor.publish('roles', function rolesPublication() {
    if (Roles.userIsInRole(this.userId, ['admin'])) {
        return Meteor.roles.find();
    }
    else {
        // user not authorized. do not publish secrets
        this.stop();
        return;
    }
});

// Meteor.publish('students', function studentsPublication() {
// 	if (Roles.userIsInRole(this.userId, ['admin'])) {
// 		return Meteor.users.find(
// 			{
// 				roles: 'student'
// 			}
// 		);
// 	}
// 	else {
// 		// user not authorized. do not publish secrets
// 		this.stop();
// 		return;
// 	}
// });

// Meteor.publish('instructors', function instructorsPublication() {
// 	if (Roles.userIsInRole(this.userId, ['admin'])) {
// 		return Meteor.users.find(
// 			{
// 				roles: 'instructor'
// 			}
// 		);
// 	}
// 	else {
// 		// user not authorized. do not publish secrets
// 		this.stop();
// 		return;
// 	}
// });