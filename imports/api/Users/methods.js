Meteor.methods({
    importFile: function(file, fileData){

        var excel;
        /**/
        var fileExtension = (/[.]/.exec(file)) ? /[^.]+$/.exec(file)[0] : undefined;
        //excel = new Excel('xlsx');
        if(fileExtension = "xlsx"){
            excel = new Excel('xlsx');
        }else {
            excel = new Excel('xls');
        }

        var workbook = excel.read(fileData , {type: 'binary'});

        var resultJson = [];

        var sheet_name_list = workbook.SheetNames;

        sheet_name_list.forEach(function(y) { /* iterate through sheets */
            var worksheet = workbook.Sheets[y];
            for (z in worksheet) {
                /* all keys that do not begin with "!" correspond to cell addresses */
                if(z[0] === '!') continue;
                var log = "Sheet: " + y + ", cell: " + z + " = " + JSON.stringify(worksheet[z].v);
                //console.log(log);
                resultJson.push(JSON.stringify(worksheet[z].v).replace(new RegExp('"', 'g'),''));
            }
        })
        var resultObject = [];
        //console.log(resultJson);
        for (var i = 1; i < resultJson.length ; i++){
            if(i % 6 == 0){
                resultJson.emails = [
                    {
                        address: resultJson[i],
                        verified: false
                    }
                ];
                var object = { createdAt: new Date() , emails: resultJson.emails, profile:{firstName : resultJson[i+1]
                    , lastName: resultJson[i+2] , schoolId: resultJson[i+4] , courseIds: [resultJson[i+5]]}, roles: [resultJson[i+3]]}
                check(object, Schema.User);
                resultObject.push(object);
            }
        }
        if(!this.userId || !Roles.userIsInRole(this.userId, ['admin'])) {
            throw new Meteor.Error('not-authorized');
        }
        for (var i = 0; i< resultObject.length;i++){
            var newUserId = Meteor.users.insert(resultObject[i]);
            console.log(newUserId);
            console.log(resultObject[i].profile.lastName);
            Accounts.setPassword(newUserId, resultObject[i].profile.lastName);
        }

        return resultObject;
    },
	'addUser'(userObject) {
		userObject.createdAt = new Date();

		userObject.emails = [
			{
				address: userObject.email,
				verified: false
			}
		];

		delete userObject.email;

		check(userObject, Schema.User);

		// Make sure the user is logged in before inserting a task
		if(!this.userId || !Roles.userIsInRole(this.userId, ['admin'])) {
			throw new Meteor.Error('not-authorized');
		}

		var newUserId = Meteor.users.insert(userObject);

		Accounts.setPassword(newUserId, userObject.profile.lastName);

		var userLanguage = navigator.language || navigator.userLanguage;
        console.log(userLanguage);
        if(userLanguage == "fr" || userLanguage=="fr-FR"){
            alert('Votre message a bien été publié');
        }
        else {
            alert('Your message is posted successfully');
        }

    },

    'randomPassword'() {
        var length = 8,
            charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
            retVal = "";
        for (var i = 0, n = charset.length; i < length; ++i) {
            retVal += charset.charAt(Math.floor(Math.random() * n));
        }
        return retVal;
    }
});