/**
 * Created by TranUyDu on 28/04/2017.
 */
/**
 * Created by TranUyDu on 26/04/2017.
 */
/*Meteor.publish('fileCollection', function debatesPublication() {
 if (Roles.userIsInRole(this.userId, ['admin'])) {
 return Debates.find();
 }
 else if (Roles.userIsInRole(this.userId, ['instructor'])) {
 var courseIds = Meteor.users.findOne({_id: this.userId}, {fields: {_id: 0, 'profile.courseIds': 1}}).profile.courseIds;
 return Debates.find({
 courseId : { $in: courseIds }
 });
 }
 else {
 // user not authorized. do not publish secrets
 this.stop();
 return;
 }
 });*/
// This code only runs on the server
Meteor.publish('fileCollection', function fileCollectionPublication() {
    return FileCollection.find();
});

Meteor.publish('files', function filePublication() {
    return FileFS.find();
});

Meteor.publish('imageCollection', function fileCollectionPublication() {
    return FileCollection.find();
});

Meteor.publish('images', function filePublication() {
    return FileFS.find();
});