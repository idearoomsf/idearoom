/**
 * Created by TranUyDu on 16/05/2017.
 */
/**
 * Created by TranUyDu on 26/04/2017.
 */
import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import './methods.js';

ImageFS = new Mongo.Collection('images');

ImageCollection = new FS.Collection("imageCollection", {
    stores: [new FS.Store.FileSystem("imageCollection"
        , { path: "~/Desktop/Stage/IdeaRoom_2_5/imports/api/Files/Uploads"})],
});

ImageCollection.allow({
    insert: function() {
        return true;
    },
    update: function() {
        return true;
    },
    remove: function() {
        return true;
    },
    download: function() {
        return true
    }
});


ImageFS.schema = new SimpleSchema({
    url: {
        type: String,
        label: "File url"
    },
    debateId: {
        type: String,
        label: "Debate id"
    },
    name:{
        type: String,
        label: "File name"
    }
});

ImageFS.attachSchema(ImageFS.schema);

ImageFS.allow({
    insert: function() {
        return true;
    },
    update: function() {
        return true;
    },
    remove: function() {
        return true;
    }
})