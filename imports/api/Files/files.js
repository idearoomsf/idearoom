/**
 * Created by TranUyDu on 26/04/2017.
 */
import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import './methods.js';

FileFS = new Mongo.Collection('files');

FileCollection = new FS.Collection("fileCollection", {
    stores: [new FS.Store.FileSystem("fileCollection"
        , { path: "~/Desktop/Stage/IdeaRoom_2_5/imports/api/Files/Uploads"})]
});

FileCollection.allow({
    insert: function() {
        return true;
    },
    update: function() {
        return true;
    },
    remove: function() {
        return true;
    },
    download: function() {
        return true
    }
});


FileFS.schema = new SimpleSchema({
    url: {
        type: String,
        label: "File url"
    },
    debateId: {
        type: String,
        label: "Debate id"
    },
    name:{
        type: String,
        label: "File name"
    }
});

FileFS.attachSchema(FileFS.schema);

FileFS.allow({
    insert: function() {
        return true;
    },
    update: function() {
        return true;
    },
    remove: function() {
        return true;
    }
})