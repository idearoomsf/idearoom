// Courses.helpers({
// 	school() {
// 		return Schools.find({_id: this.school}, {sort: {name: 1}});
// 	}
// });

Meteor.methods({
	'addCourse'(courseObject) {
		courseObject.createdAt = new Date();
        
        // Retrieve informations about the course
        var newCourse = {
            name: courseObject.name,
            schoolId: courseObject.schoolId
        };

		check(newCourse, Courses.schema);

		// Make sure the user is logged in before inserting a task
		if(!this.userId || !Roles.userIsInRole(this.userId, ['admin'])) {
			throw new Meteor.Error('not-authorized');
		}
        
        // Add course to the collection
		Courses.insert(newCourse,function(error,result){
            if(error == undefined){ // Check the state of the insertion
                // Add course id to the profile of all the users 
                Meteor.users.update({ _id: courseObject.instructorId},{ $push: {'profile.courseIds': result}}); 
                
                for(i=0; i<courseObject.usersIds.length; i++){
                    Meteor.users.update({ _id: courseObject.usersIds[i]},{ $push: {'profile.courseIds': result}});
                }
            }
        });

	}
});