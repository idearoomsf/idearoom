import { Mongo } from 'meteor/mongo';
import { SimpleSchema } from 'meteor/aldeed:simple-schema';

import './methods.js';

Courses = new Mongo.Collection('courses');

Courses.allow(
    {
        insert: function() {
            return true;
        }
    }
)

Courses.schema = new SimpleSchema({
    name: {
        type: String,
        label: "Name",
        unique: true
    },
    schoolId: {
        type: String,
        label: "School",
        // regex: SimpleSchema.Regex.Id
    }
});

Courses.attachSchema(Courses.schema);

Courses.helpers({
    school() {
        return Schools.findOne(
            {
                _id: this.schoolId
            }
        );
    }
});