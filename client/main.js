import '/imports/startup/client';

Meteor.startup(function() {
    
    var connectStatus = false;
    
    //Track user login status and redirect to main page if user log out
    
    Tracker.autorun(function() {
        if(!Meteor.userId()) {
            FlowRouter.go('/');
            console.log('User logged out');
        }
    })
})